const url = "https://treinamentoajax.herokuapp.com/messages"
const msgFeed = document.getElementById('msgFeed')
const msgTemplate = document.getElementById('msgTemplate')
const editBoxTemplate = document.getElementById('editBoxTemplate')


// Cria uma instância do template de mensagem e adiciona no feed.
// Ele seta o nome, corpo e no container, seta um data-property com o id da mensagem para uso futuro.
function displayMessage(msg) {
    const msgFragment = msgTemplate.content.cloneNode(true)
    const msgBlock = msgFragment.querySelector('.msg-block')
    msgFragment.querySelector('.msg-name').textContent = msg['name']
    msgFragment.querySelector('.msg-body').textContent = msg['message']
    msgFragment.querySelector('.msg-block').dataset.msgId = msg['id']

    msgFragment.querySelector('.msg-delete-btn')
    .addEventListener('click', () => deleteMessage(msgBlock))
    
    msgFragment.querySelector('.msg-edit-btn')
    .addEventListener('click', () => openEditBox(msgBlock))
    
    msgFeed.appendChild(msgFragment)
}

// Chamado pelo botão delete de cada mensagem
function deleteMessage(msgBlock) {
    const msgId = msgBlock.dataset.msgId

    fetch(url + '/' + msgId, { method: 'DELETE' })
        .then(console.log)
        .catch(console.warn)

    msgBlock.remove()
}

// Recebe um bloco de mensagem .msg-block e atualiza pelas informações contidas no input no interior dele
function editMessage(msgBlock) {
    const id = msgBlock.dataset.msgId
    const name = msgBlock.querySelector('[name=name]').value
    const text = msgBlock.querySelector('[name=text]').value

    let fetchBody = {"message": {}}
    if (name) fetchBody.message['name'] = name
    if (text) fetchBody.message['message'] = text

    const fetchConfig = {
        "method": "PUT",
        "headers": { "Content-Type": "application/JSON" },
        "body": JSON.stringify(fetchBody)
    }

    fetch(url + '/' + id, fetchConfig)
    .then(console.log)
    .catch(console.warn)

    setTimeout(refreshMessages, 1000)
}

// Abre uma nova caixa de edição dentro de uma caixa de mensagem. 
// É chamado diretamente pelo botão 'Editar mensagem'.
function openEditBox(containerMsg) {
    if (containerMsg.querySelector('.inputBox')) return
    
    const editBoxFragment = editBoxTemplate.content.cloneNode(true)
    const editBoxElement = editBoxFragment.querySelector('.inputBox')
    const nameInput = editBoxElement.querySelector('[name=name]')
    const textInput = editBoxElement.querySelector('[name=text]')

    nameInput.value = containerMsg.querySelector('.msg-name').innerText
    textInput.value = containerMsg.querySelector('.msg-body').innerText

    editBoxFragment.querySelector('.cancelBtn').addEventListener('click', () => editBoxElement.remove())
    editBoxFragment.querySelector('.editBtn').addEventListener('click', () => editMessage(containerMsg))

    containerMsg.appendChild(editBoxFragment)
}


/* 
Existem casos como esse em que a ideia é fazer um código mais sequencial, isso é,
primeiro preparar a mensagem para depois limpar o container, que eu acho que fica
mais bonitinho fazer usando async/await. 
*/
async function refreshMessages() {
    const resp = await fetch(url).catch(console.warn)
    const json = await resp.json().catch(console.warn)
    msgFeed.innerHTML = ''
    for (const msg of json) {
        displayMessage(msg)
    }
}


async function getSpecificMessage() {
    const selectedId = document.getElementById('idSearch-input').value || 0

    const resp = await fetch(url + '/' + selectedId).catch(console.warn)
    if(!resp.ok) return
    
    const json = await resp.json()
    msgFeed.innerHTML = ''
    displayMessage(json)
}


function submitMessage() {
    const nameInput = document.getElementById('inputName')
    const msgInput = document.getElementById('inputMessage')

    const fetchBody = {
        "message": {
            "name": nameInput.value,
            "message": msgInput.value
        }
    }
    const fetchConfig = {
        "method": "POST",
        "headers": { "Content-Type": "application/JSON" },
        "body": JSON.stringify(fetchBody)
    }

    fetch(url, fetchConfig)
        .then(console.log)
        .catch(console.warn)

    nameInput.value = ''
    msgInput.value = ''
    setTimeout(refreshMessages, 1000)
}

// Definição dos botões
document.getElementById('getMessageById').addEventListener('click', getSpecificMessage)
document.getElementById('submitMsgBtn').addEventListener('click', submitMessage)
document.getElementById('toTopBtn').addEventListener('click', () => window.scrollTo(0, 0))

refreshMessages()